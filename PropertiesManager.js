import {AsyncStorage} from 'react-native'

class PropertiesManager{
  constructor(){

  }
  async set(key, value, callback = undefined)
  {
      try {
        await AsyncStorage.setItem(key, value, callback);
      } catch (error) {
        console.log("Error saving key "+key+" value "+value)
      }
    }

  async get(key) {
    try {
        const value = await AsyncStorage.getItem(key);
        return value;
     } catch (error) {
       console.log("Error getting data from key "+key)
       return undefined;
     }
  }
}

export default PropertiesManager;
