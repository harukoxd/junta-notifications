import { StyleSheet } from 'react-native';
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ff0000',
    alignItems: 'center',
    justifyContent: 'center',
    width:"100%"
  },
  rest:{
    position:"absolute",
      width:"100%",
      height:"100%",
      alignItems: 'center',
      justifyContent: 'center',
  },

  titlePhone:{
    width:"100%",
    height:"100%",
    alignItems: 'center',
    justifyContent: 'center',
    position:"absolute",
    top:0,
    left:0

  },
  phoneIcon:{
    padding:0,
    margin:0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonContainer:{
    width:"100%",
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: "#0000",
    backgroundColor:"red",

    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.3,
    shadowRadius: 12.16,
    elevation: 3,
  },
  buttonText:{
    color:"white"
  },
  textActivation:{
    textAlign:"center",
    marginTop:10
  },
  boxRest:{
    padding:"5%",
    marginBottom:"10%"
  },
  inputMovil:{
    borderColor:"black",
    borderBottomWidth:1,
    marginTop:20,
    marginBottom:20
  },
  boxContainer:{
    width:"80%",
    position:"relative",
    backgroundColor:"white"
  },
  textTop:{
    color:"white",
    marginTop:20
  },
  textVolverEnviar:{
    display:"flex",
    flexDirection:"row"
  },
  textActivationLink:{
    marginLeft:5,
    color:"#3B83E3",
    marginTop:10
  },
  backRow:{
    color:"white",
    position:"absolute",
    top:"10%",
    left:10,
    zIndex:20
  },
  top:{
    width:"100%",
    backgroundColor: '#3B83E3',
    color:"white",
    justifyContent:'center',
    alignItems:'center',
      position:"absolute",
      top:0
  },
  logoJunta:{
      position:"absolute",
      width:10,
      bottom:15,
      transform:[{translateX:-0.5}]
  }
});
export default style;
