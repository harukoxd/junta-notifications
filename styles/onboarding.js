import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width:"100%",
    height:"100%"
  },
  logo:{
    shadowColor: "#0000",
    backgroundColor:"black",
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 35,
    shadowOffset: {
    	width: 100,
    	height: 0,
    },
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 10
  },
  welcomeText:{
    color: "white",
    textAlign:"center",
    marginBottom:12
  },
  downText:{
    color: "#3B83E3",
    textAlign:"center"
  },
  welcomeContainer:{
    margin:"10%",
    marginBottom:"20%",
    marginTop:"13%"
  },
  buttonContainer:{
    width:"100%",
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius:5,
    shadowColor: "#0000",
    backgroundColor:"red",

    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.3,
    shadowRadius: 12.16,
    elevation: 3,
  },
  buttonText:{
    color:"white"
  },
  logoJunta:{
      position:"absolute",
      width:"100%",
      bottom:15,
      transform:[{translateX:-0.5}]
  },
  descContainer:{
        width:"80%",
        height:"10%",
        alignItems: 'center',
        justifyContent: 'center'
  },
  spaceTop:{
    height:"18%"
  }
});
export default style;
