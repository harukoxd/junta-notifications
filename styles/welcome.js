import { StyleSheet } from 'react-native';
const style = StyleSheet.create({
  container: {
    flex: 1,

    alignItems: 'center',
    justifyContent: 'center',
      position:"relative",
    width:"100%"
  },
  rest:{

      width:"100%",
      alignItems: 'center',
      justifyContent: 'center',



  },
  circle:{
    width:7,
    height:7,
    margin:"1.5%",
    borderRadius:24
  },
  circles:{
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:"row",
    padding:"5%",
    backgroundColor:"white"
  },
  titlePhone:{
    width:"100%",
    height:"100%",
    alignItems: 'center',
    justifyContent: 'center',
    position:"absolute",
    top:0,
    left:0

  },
  phoneIcon:{
    padding:0,
    margin:0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonContainer:{
    width:"100%",
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: "#0000",
    backgroundColor:"#ffffff",

    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.3,
    shadowRadius: 12.16,
    elevation: 3,
  },
  buttonText:{
    color:"white"
  },
  textActivation:{

  },
  boxRest:{
    padding:"5%",
    marginBottom:"10%"
  },
  boxContainer:{
    width:"80%",
    position:"relative",
  },
  backRowText:{
    color:"white",
    marginTop:20
  },
  arrowLeft:{
    margin:15,
    marginTop:"5%"
  },
  textTop:{
    color:"white",
        marginTop:20
  },
  backRow:{
    color:"white",
    position:"absolute",
    top:30,
    left:10,
    zIndex:20
  },
  top:{
    width:"100%",
    justifyContent:'center',
    alignItems:'center',
      position:"absolute",
      top:0
  },
  logoJunta:{
      position:"absolute",
      width:"100%",
      bottom:15,
      transform:[{translateX:-0.5}]
  }
});
export default style;
