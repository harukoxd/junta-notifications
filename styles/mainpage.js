import { StyleSheet } from 'react-native';
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
    width:"100%",
    display:"flex",
    flexDirection:"column",
    position:"relative"
  },
  textTop:{
    color:"white",
    margin:0,
    width:"100%",
    textAlign:"center"

  },
  searchContainer:{
    position:"relative",
    display:"flex",
    flexDirection:"row",
    backgroundColor:"#0C2340",
    width:"90%",
    height:"90%",
    borderRadius:10

  },
  lupaContainer:{
    width:"10%",
    display:"flex",
    flexDirection:"column",
    alignContent:"center",
    justifyContent:"center"
  },
  searchInput:{
    padding:0,
    flex:1

  },
  secondContainer:{
      width:"100%",
      backgroundColor: '#3B83E3',
      display:"flex",
      flexDirection:"row",
      justifyContent:"center",
      alignContent:"center",
      marginTop:13
  },
  topMainPain:{
    width:"100%",
    backgroundColor: '#3B83E3',
    color:"white",
    justifyContent:'center',
    alignItems:'center',
    position:"absolute",
    top:0,
    display:"flex",
    paddingTop:"10%",
    flexDirection:"column"

  },
  messagesContainer:{
    height:"100%"
  },
  logoJunta:{
      position:"absolute",
      width:10,
      bottom:15,
      transform:[{translateX:-0.5}]
  }
});
export default style;
