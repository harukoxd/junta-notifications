class Soap{
  constructor(){
    this.url = 'http://www.juntadeandalucia.es/servicios/desarrollo/p3s/services/P3SWS?wsdl';
    this.init = this.init.bind(this);
    this.doRequest = this.doRequest.bind(this);
    this.headRequest = this.headRequest.bind(this);
    this.endRequest = this.endRequest.bind(this);
    this.terminalRegistration = this.terminalRegistration.bind(this);
    this.init();

  }
  init(){
    var request = new XMLHttpRequest();
    var st = this;
    request.onreadystatechange = (e) => {
     if (request.readyState !== 4) {
       return;
     }
     if (request.status === 200) {
       st.source = request.responseText.split('xmlns:impl="')[1].split('"')[0];
     } else {
       alert('No estás conectado a internet o no se ha podido conectar con el servicio.');
     }
    };

    request.open('GET', this.url);
    request.send();
  }
  headRequest(){
    return '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' +
    '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" ' +
                       'xmlns:apachesoap="http://xml.apache.org/xml-soap" ' +
                       'xmlns:impl="'+this.source+'" ' +
                       'xmlns:intf="'+this.source+'" ' +
                       'xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" ' +
                       'xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" ' +
                       'xmlns:wsdlsoap="http://schemas.xmlsoap.org/wsdl/soap/" ' +
                       'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' +
                       'xmlns:tns="http://schemas.xmlsoap.org/soap/encoding/" ' +
                       'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >' +
                       '<SOAP-ENV:Body>';
  }
  endRequest(){
    return '</SOAP-ENV:Body></SOAP-ENV:Envelope>'
  }
  terminalRegistration(phone,id_device,type_device){
    return this.headRequest() +
          '<mns:p3sSolicitudRegistro xmlns:mns="http://ws.sms.sadesi" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
              '<tlfno xsi:type="xsd:string">'+
                phone +
              '</tlfno>'+
              '<id_tlfno xsi:type="xsd:string">' +
                id_device +
              '</id_tlfno>' +
              '<red xsi:type="xsd:string">' +
                type_device +
              '</red>' +
        '</mns:p3sSolicitudRegistro>' +
        this.endRequest();
  }
  checkCode(phone,id_device,pin){
    return this.headRequest() +
          '<mns:p3sValidacionRegistro xmlns:mns="http://ws.sms.sadesi" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
              '<tlfno xsi:type="xsd:string">'+
                phone +
              '</tlfno>'+
              '<id_tlfno xsi:type="xsd:string">' +
                id_device +
              '</id_tlfno>' +
              '<pin xsi:type="xsd:string">' +
                pin +
              '</pin>' +
        '</mns:p3sValidacionRegistro>' +
        this.endRequest();
  }
  subscriptions(phone){
    return this.headRequest() +
          '<mns:p3sObtieneFuentes xmlns:mns="http://ws.sms.sadesi" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
              '<tlfno xsi:type="xsd:string">'+
                phone +
              '</tlfno>'+
        '</mns:p3sObtieneFuentes>' +
        this.endRequest();
  }
  doRequest(xml){
    var ts = this;
    return new Promise(function (resolve){fetch(ts.url,{
      method: 'POST',
      headers: {
        'Content-Type': 'text/xml',
        'SOAPAction':'add'
      },
      body:xml
    })
        .then((response) => resolve(response.text()))
        .catch((error) => {
          resolve(error);
        });
      }
    );
  }
}
export default Soap;
