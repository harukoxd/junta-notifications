import React from 'react';
import Onboarding from './screens/Onboarding.js'
import MainPage from './screens/MainPage.js'
import Welcome from './screens/Welcome.js'
import Activacion from './screens/Activacion.js'
import Soap from './Soap.js'

import FirstBoard from './screens/FirstBoard.js'
import { View,StyleSheet, Text } from 'react-native';
import PropertiesManager from './PropertiesManager'
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    height:'100%',
    width:'100%'
  },
});

class ScreenManager extends React.Component{
  constructor(props){
    super(props);
    this.state = {page:0}
    this.propertiesManager = new PropertiesManager();
    this.changePage= this.changePage.bind(this)
    this.soap = new Soap();

  }
  changePage(page,args){
    if(page == "activation")
      this.setState({page:page,phone:args[0]});
    else
      this.setState({page:page});
  }
  async componentWillMount(){
    var data = await this.propertiesManager.get('userId');
    if(data != undefined){
      this.setState({page:"onboarding"})
    }
    else{
        this.setState({page:"onboarding"})
    }
  }
  render(){
    let content;
    if(this.state.page == "main_page")
      content = <MainPage soap={this.soap} action={this.changePage} />
    else if(this.state.page  == "first_board")
      content = <FirstBoard soap={this.soap} action={this.changePage} />
    else if(this.state.page  == "welcome")
      content = <Welcome soap={this.soap} action={this.changePage} />
    else if(this.state.page  == "activation")
      content = <Activacion soap={this.soap} phone={this.state.phone} action={this.changePage} />
    else if(this.state.page  == "onboarding")
      content = <Onboarding action={this.changePage} />
    return (
      <View style={style.container}>
        {content}
      </View>
    )
  }
}
export default ScreenManager
