import React from 'react';
import ScreenManager from './ScreenManager';
import KeyboardShift from './KeyboardShift';

export default function App() {
  return (
    <KeyboardShift>
    {() => (
      <ScreenManager />
    )}
    </KeyboardShift>
  );
}
