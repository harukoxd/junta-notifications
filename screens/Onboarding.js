import React from 'react';
import { Text, View,TouchableHighlight,BackHandler } from 'react-native';
import styles from '../styles/onboarding.js'
import {LinearGradient} from 'expo-linear-gradient';
import Logo from '../assets/svgs/logo.js'
import LogoJunta from '../assets/svgs/junta_blue.js'

import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';

class Onboarding extends React.Component{
  constructor(props){
    super(props);
    this.action = props.action;
    this.goFirstBoard = this.goFirstBoard.bind(this);
    this.backPage = this.backPage.bind(this)
  }
  backPage(){
    return true;
  }
  goFirstBoard(){
    this.action('first_board',[])
  }
  componentDidMount(){
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.backPage);

  }
  render(){
    return (
      <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.container}>
          <View style={styles.spaceTop}></View>
          <View style={[styles.logo,{width:vmin(35),height:vmin(35)}]}>
            <Logo />
          </View>

          <View style={styles.welcomeContainer}>
            <Text style={[styles.welcomeText, {fontSize:vmin(11)}]}>Bienvenid@</Text>
            <Text style={[styles.downText, {fontSize:vmin(5)}]}>A la aplicación de mensajería móvil de la Junta de Andalucía</Text>
          </View>
          <TouchableHighlight onPress={this.goFirstBoard} style={{width:"80%"}}>
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#44b9fd', '#4f89fd']} style={[styles.buttonContainer,{height:vmin(15)}]}>
              <Text style={[styles.buttonText,{fontSize:vmin(4)}]}>EMPEZAR</Text>
            </LinearGradient>
          </TouchableHighlight>
          <View style={[styles.descContainer, {marginBottom:vmin(20)}]}>
            <Text style={[styles.downText, {fontSize:vmin(4)}]}>Pulse "EMPEZAR" para comenzar el proceso de activación del producto</Text>
          </View>
          <View style={[styles.logoJunta,{width:"100%",height:vmin(10)}]}>
            <LogoJunta color="#3B83E3" />
          </View>
      </LinearGradient>
    )
  }
}
export default Onboarding
