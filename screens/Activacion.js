import React from 'react';
import { Text, View, TextInput,TouchableHighlight,Platform,BackHandler  } from 'react-native';
import styles from '../styles/firstboard.js'
import Constants from 'expo-constants';
import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';
import PhoneActivation from '../assets/svgs/phone_activation.js'
import LogoJunta from '../assets/svgs/junta_blue.js'
import {LinearGradient} from 'expo-linear-gradient';
import Soap from '../Soap.js'
import PropertiesManager from '../PropertiesManager'
class Activacion extends React.Component{
  constructor(props){
    super(props);
    this.state = {text:'',disable:true};
    this.handleInputChange = this.handleInputChange.bind(this);
    this.registerDevice = this.registerDevice.bind(this);
    this.checkCode = this.checkCode.bind(this);
    this.action = props.action;
    this.phone = props.phone;
    this.soap = props.soap;
    this.backPage = this.backPage.bind(this)
  }
  backPage(){
    this.action('first_board',[]);
    return true;
  }
  async checkCode(){
    if(!this.state.disable){
      var rq = this.soap.checkCode(this.phone,Constants.deviceId, this.state.text)
      var dt = await this.soap.doRequest(rq);
      if(dt.includes('p3sValidacionRegistroReturn')){
        var validationCode = dt.split('<p3sValidacionRegistroReturn xsi:type="xsd:string">')[1].split('</p3sValidacionRegistroReturn>')[0]
        var propertiesManager = new PropertiesManager();
        var save = await propertiesManager.set('phone',this.phone);
        var save = await propertiesManager.set('userId',validationCode);
        this.action('welcome',[]);
      }
      else if(dt.includes('sadesi.sms.ws.InvalidCredentialsException')) {
        alert("Código incorrecto")
      }
      else{
        alert(dt["message"])
      }
    }


  }
  handleInputChange(text) {
      const filteredText = text.replace(/\D/gm, '');
      if(filteredText !== text) {
        this.setState({ text: filteredText,disable:filteredText.length != 4 });
      } else {
        this.setState({ text: filteredText,disable:filteredText.length != 4 });
      }
  }
  async registerDevice(){

    this.action('first_board',[]);

  }
  componentDidMount(){
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.backPage);

  }
  render(){
    var linearGradient = ['#44b9fd', '#4f89fd'];
    var colorText ="white"

    if(this.state.disable){
      linearGradient = ['#EEEEEE', '#EEEEEE'];
      colorText = "#6B707C"
    }
    return (
      <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']}  style={styles.container}>
        <View style={[styles.top,{height:vmin(17)}]}>
          <Text style={[styles.textTop, {fontSize:vmin(4)}]}>AVISOS Junta de Andalucía</Text>
        </View>
        <View style={[styles.rest,{marginBottom:vmin(20),marginTop:vmin(17)}]}>
          <View style={styles.boxContainer}>
            <View style={[styles.titlePhone,{height:vmin(32),transform:[{translateY:vmin(-8)}]}]}>
              <View  style={[styles.phoneIcon,{height:vmin(32),width:vmin(32)}]}>
                <PhoneActivation />
              </View>
            </View>
            <View style={styles.boxRest}>
              <Text style={[styles.textMovil, {fontSize:vmin(4), marginTop:vmin(20)}]}>Hemos enviado un SMS con un código de activación. Tecléalo y pulsa 'Confirmar'</Text>
              <TextInput onChangeText={this.handleInputChange} value={this.state.text} keyboardType='numeric' underlineColorAndroid='transparent' textContentType='telephoneNumber' style={[styles.inputMovil, {fontSize:vmin(4)}]} placeholder="Código de activación" placeholderTextColor="black" />
              <View style={[styles.textVolverEnviar]}>
                <Text style={[styles.textActivation, {fontSize:vmin(4)}]}>¿No recibiste el SMS?</Text>
                  <TouchableHighlight onPress={this.registerDevice}><Text style={[styles.textActivationLink, {fontSize:vmin(4)}]}>Volver a enviar</Text></TouchableHighlight>
              </View>
            </View>
            <TouchableHighlight onPress={this.checkCode} style={{width:"100%"}}>
              <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={linearGradient} style={[styles.buttonContainer,{height:vmin(15)}]}>
                <Text style={[styles.buttonText,{fontSize:vmin(4),color:colorText}]}>ENVIAR</Text>
              </LinearGradient>
            </TouchableHighlight>
          </View>
        </View>
        <View style={[styles.logoJunta,{width:"100%",height:vmin(10)}]}>
          <LogoJunta color="#3B83E3" />
        </View>
      </LinearGradient>
    )
  }
}
export default Activacion
