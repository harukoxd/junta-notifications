import React from 'react';
import { Text, View, TextInput,TouchableHighlight,Platform,BackHandler  } from 'react-native';
import styles from '../styles/firstboard.js'
import Constants from 'expo-constants';
import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';
import PhoneRegister from '../assets/svgs/phone_register.js'
import LogoJunta from '../assets/svgs/junta_blue.js'
import {LinearGradient} from 'expo-linear-gradient';
class FirstBoard extends React.Component{
  constructor(props){
    super(props);
    this.state = {text:'',disable:true};
    this.handleInputChange = this.handleInputChange.bind(this);
    this.registerDevice = this.registerDevice.bind(this);
    this.action = props.action;
    this.soap = props.soap;
    this.backPage = this.backPage.bind(this)
  }
  backPage(){
    return true;
  }
  handleInputChange(text) {
      const filteredText = text.replace(/\D/gm, '');
      if(filteredText !== text) {
        this.setState({ text: filteredText,disable:filteredText.length != 9 });
      } else {
        this.setState({ text: filteredText,disable:filteredText.length != 9 });
      }
  }
  componentDidMount(){
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.backPage);

  }
  async registerDevice(){
    if(!this.state.disable){
      var rq = this.soap.terminalRegistration(this.state.text,Constants.deviceId, Platform.OS)
      var dt = await this.soap.doRequest(rq);

      if(dt.includes('p3sSolicitudRegistroReturn')){
        this.action('activation',[this.state.text])
      }
      else{
        alert(dt)
      }

    }

  }
  render(){
    var linearGradient = ['#44b9fd', '#4f89fd'];
    var colorText ="white"
    if(this.state.disable){
      linearGradient = ['#EEEEEE', '#EEEEEE'];
      colorText = "#6B707C"
    }
    return (
      <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']}  style={styles.container}>
        <View style={[styles.top,{height:vmin(17)}]}>
          <Text style={[styles.textTop, {fontSize:vmin(4)}]}>AVISOS Junta de Andalucía</Text>
        </View>
    
        <View style={[styles.rest,{marginBottom:vmin(20),marginTop:vmin(17)}]}>
          <View style={styles.boxContainer}>
            <View style={[styles.titlePhone,{height:vmin(32),transform:[{translateY:vmin(-8)}]}]}>
              <View  style={[styles.phoneIcon,{height:vmin(32),width:vmin(32)}]}>
                <PhoneRegister />
              </View>
            </View>
            <View style={styles.boxRest}>
              <Text style={[styles.textMovil, {fontSize:vmin(4), marginTop:vmin(20)}]}>MÓVIL</Text>
              <TextInput onChangeText={this.handleInputChange} value={this.state.text} keyboardType='numeric' underlineColorAndroid='transparent' textContentType='telephoneNumber' style={[styles.inputMovil, {fontSize:vmin(4)}]} placeholder="Número de móvil" placeholderTextColor="black" />
              <Text style={[styles.textActivation, {fontSize:vmin(4)}]}>Te enviaremos un SMS con el código de activación para darte de alta en el servicio</Text>
            </View>
            <TouchableHighlight onPress={this.registerDevice} style={{width:"100%"}}>
              <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={linearGradient} style={[styles.buttonContainer,{height:vmin(15)}]}>
                <Text style={[styles.buttonText,{fontSize:vmin(4),color:colorText}]}>ENVIAR</Text>
              </LinearGradient>
            </TouchableHighlight>
          </View>
        </View>
        <View style={[styles.logoJunta,{width:"100%",height:vmin(10)}]}>
          <LogoJunta color="#3B83E3" />
        </View>
      </LinearGradient>
    )
  }
}
export default FirstBoard
