import React from 'react';
import { Text, View, TextInput,TouchableHighlight,Platform,BackHandler  } from 'react-native';
import styles from '../styles/mainpage.js'
import Constants from 'expo-constants';
import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';
import PhoneRegister from '../assets/svgs/phone_register.js'
import LogoJunta from '../assets/svgs/junta_blue.js'
import Logo from '../assets/svgs/logo.js'

import {LinearGradient} from 'expo-linear-gradient';
class MainPage extends React.Component{
  constructor(props){
    super(props);
    this.state = {search:''};
    this.handleInputChange = this.handleInputChange.bind(this);
    this.action = props.action;
    this.soap = props.soap;
    this.backPage = this.backPage.bind(this)
  }
  backPage(){
    return true;
  }
  handleInputChange(text) {
      this.setState({search:text})
  }
  componentDidMount(){
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.backPage);

  }

  render(){
    var colorText ="white"
    if(this.state.disable){
      linearGradient = ['#EEEEEE', '#EEEEEE'];
      colorText = "#6B707C"
    }
    return (
      <View  style={styles.container}>
        <View style={styles.topMainPain}>
          <Text style={[styles.textTop, {fontSize:vmin(4)}]}>Inicio</Text>
          <View style={styles.secondContainer}>
            <View style={styles.searchContainer}>
              <TextInput style={[styles.searchInput, {fontSize:vmin(4)}]} onChangeText={this.handleInputChange} value={this.state.search} />
              <View  style={styles.lupaContainer}>
                <Logo />
              </View>
            </View>
          </View>
        </View>
        <View style={styles.messagesContainer}>
        </View>
        <View style={[styles.logoJunta,{width:"100%",height:vmin(10)}]}>
          <LogoJunta color="#3B83E3" />
        </View>
      </View>
    )
  }
}
export default MainPage
