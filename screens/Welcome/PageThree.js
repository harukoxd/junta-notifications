import React from 'react';
import { Text, View } from 'react-native';
import styles from './css/pages.js'
import FirstPageSvg from '../../assets/svgs/FirstPageSvg.js'

import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';
class PageThree extends React.Component{
  constructor(props){
    super(props);
    this.soap = props.soap;
  }
  render(){
    return (
      <View style={styles.containerPage}>
        <View style={[styles.imageContainer,{width:vmin(65),height:vmin(65)}]}>
          <FirstPageSvg />
        </View>
        <Text style={[styles.title, {fontSize:vmin(4)}]}>COMPLETA EL FORMULARIO DE ALTA</Text>
        <Text style={[styles.text, {fontSize:vmin(4)}]}>Rellena el formulario con tus datos para comenzar a recibir avisos de las fuentes de información.</Text>
      </View>
    )
  }
}
export default PageThree
