import React from 'react';
import { Text, View } from 'react-native';
import styles from './css/pages.js'
import FirstPageSvg from '../../assets/svgs/FirstPageSvg.js'

import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';
class PageOne extends React.Component{
  constructor(props){
    super(props);
    this.soap = props.soap;
  }
  render(){
    return (
      <View style={styles.containerPage}>
        <View style={[styles.imageContainer,{width:vmin(65),height:vmin(65)}]}>
          <FirstPageSvg />
        </View>
        <Text style={[styles.title, {fontSize:vmin(4)}]}>SELECCIONA LA ENTIDAD A LA QUE QUIERAS SUBSCRIBIRTE</Text>
        <Text style={[styles.text, {fontSize:vmin(4)}]}>Para comenzar a recibit mensajes de Avisos Junta deberás darte de alta en las distintas fuentes de información de tu interés.</Text>
      </View>
    )
  }
}
export default PageOne
