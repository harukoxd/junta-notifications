import { StyleSheet } from 'react-native';
const style = StyleSheet.create({
  containerPage: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    position:"relative",
    backgroundColor:"white",
  },
  serviceContainer:{
      width:"100%",
      paddingLeft:"5%",
        paddingRight:"5%",
        marginBottom:10
  },
  imageContainer:{
    alignItems: 'center',
    justifyContent: 'center',
      paddingTop:"10%"
  },
  titleAltaText:{
      padding:"10%",
          color:"#6b707C",
  },
  text:{
    textAlign:"center",
    color:"#6b707C",
      padding:"4%",
      marginLeft:"7%",
      marginRight:"7%"
  },
  paisContainer:{
    width:"90%",
    flexDirection:"row",
    justifyContent:"space-between"
  },
  inputs:{
    borderBottomWidth: 1,
        borderBottomColor: '#6b707C',
        width:"90%",
        padding:"2%"


  },
  inputsAux:{
    borderBottomWidth: 1,
        borderBottomColor: '#6b707C',
        width:"45%",
  padding:"2%"

  },
  title:{
    textAlign:"center",
    color:"#000000",
    padding:"4%",
    marginLeft:"7%",
    marginRight:"7%",

  },
  textAux:{
      color:"#6b707C",
      width:"65%",
      textAlign:"center",
      margin:"7%"
  },
  top:{
    flexDirection:"row",
    width:"100%",
    justifyContent:"space-between",

    color:"#3B83E3"
  },
  textTop:{
        color:"#3B83E3",
        padding:"5%"
  }
});
export default style;
