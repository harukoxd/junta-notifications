import React from 'react';
import { Text, View,TouchableWithoutFeedback, ScrollView } from 'react-native';
import styles from './css/pages.js'
import Logo from '../../assets/svgs/logo.js'
import Service from './components/service.js'
import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';


class PageTwo extends React.Component{
  constructor(props){
    super(props);
    this.soap = props.soap;

    this.serviceManager = props.serviceManager;
    this.unselectAll = this.unselectAll.bind(this)
    this.selectAll = this.selectAll.bind(this)
    this.state = {services:[]}
  }
  unselectAll(){
    for(var i = 0;i< this.serviceManager.listServices.length;i++)
    {
      this.myRefs[i].current.unselect();
    }
  }
  selectAll(){
    for(var i = 0;i< this.serviceManager.listServices.length;i++)
    {
      this.myRefs[i].current.select();
    }
  }
  componentDidMount(){

    this.myRefs = [];
    for(var i = 0;i< this.serviceManager.listServices.length;i++)
    {
      this.myRefs.push(React.createRef());
    }
    this.setState({services:this.serviceManager.listServices})
  }
  render(){
    let options = this.state.services.map((item, i) =>
       <Service soap={this.soap} key={i} data={item} ref={this.myRefs[i]} />
   )
    return (
      <View style={[styles.containerPage, {maxHeight:vh(55)}]}>
        <View style={styles.top}>
          <TouchableWithoutFeedback onPress={this.unselectAll}>
              <Text style={[styles.textTop, {fontSize:vmin(4)}]}>LIMPIAR</Text>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={this.selectAll}>
                  <Text style={[styles.textTop, {fontSize:vmin(4)}]}>SELECCIONAR TODO</Text>
          </TouchableWithoutFeedback>
        </View>
        <ScrollView style={styles.serviceContainer}>
          {options}
        </ScrollView>
      </View>
    )
  }
}
export default PageTwo
