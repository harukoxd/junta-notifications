import React from 'react';
import { Text, View,TouchableWithoutFeedback } from 'react-native';
import styles from './css/service.js'
import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';
import Logo from '../../../assets/svgs/logo.js'

class Service extends React.Component{
  constructor(props){
    super(props);
    this.state = {selected:false}
    this.data = props.data;
    this.name = this.data.desc;
    this.changeSelection = this.changeSelection.bind(this);
    this.unselect = this.unselect.bind(this);
    this.select = this.select.bind(this);

  }
  unselect(){
    this.setState({selected:false})
  }
  select(){
    this.setState({selected:true})
  }
  changeSelection(){
    this.setState({selected:!this.state.selected})
  }
  render(){
    return (
      <View style={{width:"100%"}}>
      <View style={styles.containerService}>
        <Text style={[styles.title, {fontSize:vmin(4)}]}>{this.name}</Text>
        <TouchableWithoutFeedback style={styles.tick} onPress={this.changeSelection}>

            {this.state.selected ? (<View style={styles.selected}><Logo /></View>) : (<View style={styles.unselected}></View>)}

        </TouchableWithoutFeedback>
      </View>
      </View>
    )
  }
}
export default Service
