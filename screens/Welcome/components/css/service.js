import { StyleSheet } from 'react-native';
const style = StyleSheet.create({
  containerService: {


     borderBottomWidth: 1,
         borderBottomColor: '#6b707C',
     padding:"5%",
     flexDirection:"row",



  },
  title:{
    color:"black",
    width:"95%",
    textAlign:"left",
    justifyContent:"center",
    alignItems:"center",
       flexDirection:"row",
       height:"100%"
  },
  tick:{
    height:"100%",
    backgroundColor:"red"

  },
  selected:{
    backgroundColor:"#3B83E3",
    borderRadius:25,
    width:18,
    height:18,
    justifyContent:"center",
    alignItems:"center",
    padding:3
  },
  unselected:{
    backgroundColor:"transparent",
    borderWidth: 1,
        borderColor: '#6b707C',
    borderRadius:25,
    width:18,
    height:18,

  }
});
export default style;
