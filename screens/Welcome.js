import React from 'react';
import { Text, View, TextInput,TouchableHighlight,Platform,TouchableWithoutFeedback,BackHandler  } from 'react-native';
import styles from '../styles/welcome.js'
import Constants from 'expo-constants';
import {vw, vh, vmin, vmax} from 'react-native-expo-viewport-units';
import Logo from '../assets/svgs/logo.js'
import LogoJunta from '../assets/svgs/junta_blue.js'
import ArrowLeft from '../assets/svgs/ArrowLeft.js'

import {LinearGradient} from 'expo-linear-gradient';
import PageOne from './Welcome/PageOne'
import PageTwo from './Welcome/PageTwo'
import PageThree from './Welcome/PageThree'
import PageFour from './Welcome/PageFour'
import PropertiesManager from '../PropertiesManager'
import convert from 'xml-js';
import ServicesManager from '../ServicesManager.js'
class MainPage extends React.Component{
  constructor(props){
    super(props);
    this.state = {text:'',disable:true,page:0};
    this.action = props.action;
    this.soap = props.soap;
    this.propertiesManager = new PropertiesManager();
    this.nextPage = this.nextPage.bind(this);
    this.backPage = this.backPage.bind(this);
    this.formulario = React.createRef();
  }

  backPage(){
    if(this.state.page > 0)
    this.setState({page:this.state.page-1})
    else
    this.action('first_board',[])
    return true;
  }
  nextPage(){
    if(this.state.page < 3)
    this.setState({page:this.state.page+1})
    if(this.state.page == 3){
      var result = this.formulario.current.saveFormulario();
      if(result){
         this.action('main_page',[])
      }
    }
  }
  componentWillUnmount() {
    this.backHandler.remove()
  }
  async componentDidMount(){
    var data = await this.propertiesManager.get('phone');
    var request = this.soap.subscriptions(data)
    var result = await this.soap.doRequest(request);
    var options = {compact: true, ignoreComment: true, spaces: 4};
    var json = JSON.parse(convert.xml2json(result, options))
    this.serviceManager = new ServicesManager(json);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.backPage);

  }
  render(){
    let content;
    var ftClass = "#6B707C"
    var ndClass = "#6B707C"
    var rdClass = "#6B707C"
    var thClass = "#6B707C"

    if(this.state.page == 0)
    {
      content = <PageOne soap={this.soap} />
      ftClass = "#3B83E3"
    }
    else if(this.state.page == 1)
    {
      content = <PageTwo soap={this.soap} serviceManager={this.serviceManager} />
      ndClass = "#3B83E3"
    }
    else if(this.state.page == 2){
      content = <PageThree soap={this.soap} />
      rdClass = "#3B83E3"
    }
    else{
      content = <PageFour soap={this.soap} ref={this.formulario} />
      thClass = "#3B83E3"
    }
        var linearGradient = ['#44b9fd', '#4f89fd'];
    return (
      <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']}  style={styles.container}>
        <View style={[styles.top,{height:vmin(17)}]}>
          <Text style={[styles.textTop, {fontSize:vmin(4)}]}>{this.state.page != 3 ? "Proceso de alta" : "Formulario de alta"}</Text>
        </View>
        <View style={[styles.top,{height:vmin(17),justifyContent:"center", alignItems:"flex-start"}]}>
          <TouchableHighlight onPress={this.backPage} style={[styles.arrowLeft,{width:vmin(6)}]}>
              <ArrowLeft color="white" />
          </TouchableHighlight>
        </View>
        <View style={[styles.rest]}>
          <View style={styles.boxContainer}>
            {content}
            <View style={styles.circles}>
                <View style={[styles.circle,{backgroundColor:ftClass}]} />
                <View style={[styles.circle,{backgroundColor:ndClass}]} />
                <View style={[styles.circle,{backgroundColor:rdClass}]} />
                <View style={[styles.circle,{backgroundColor:thClass}]} />
            </View>
            <TouchableHighlight onPress={this.nextPage} style={{width:"100%"}}>
              <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={linearGradient} style={[styles.buttonContainer,{height:vmin(15)}]}>
                <Text style={[styles.buttonText,{fontSize:vmin(4),color:"white"}]}>{this.state.page != 3 ? "SIGUIENTE" : "DARME DE ALTA Y RECIBIR MENSAJES"}</Text>
              </LinearGradient>
            </TouchableHighlight>
          </View>
        </View>
        <View style={[styles.logoJunta,{width:"100%",height:vmin(10)}]}>
          <LogoJunta color="white" />
        </View>
      </LinearGradient>
    )
  }
}
export default MainPage
