import React from 'react'
import Svg, { G, Path } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: title */

const JuntaBlue = props => (
  <Svg width="100%"
  height="100%"
  viewBox="0 0 48 24" {...props}>
    <G fill={props.color} fillRule="evenodd">
      <Path d="M24.003 3.487c7.422.014 14.835 2.725 20.497 8.132a28.254 28.254 0 013.5 4.033c-1.26-3.07-3.186-5.944-5.784-8.424C37.184 2.423 30.594.014 24 0 17.404.014 10.816 2.423 5.784 7.228 3.186 9.708 1.26 12.583 0 15.652a28.245 28.245 0 013.5-4.033C9.162 6.212 16.582 3.5 24.003 3.487" />
      <Path d="M26.966 24h-6.472l3.236-10.435z" />
      <Path d="M24.275 10.326c5.814.015 11.62 2.157 16.055 6.427.838.806 1.597 1.66 2.277 2.551a18.614 18.614 0 00-4.58-7.013c-3.8-3.659-8.777-5.495-13.758-5.508-4.98.013-9.957 1.85-13.757 5.508a18.616 18.616 0 00-4.58 7.013 22.353 22.353 0 012.277-2.551c4.436-4.27 10.253-6.412 16.066-6.427" />
    </G>
  </Svg>
)

export default JuntaBlue
