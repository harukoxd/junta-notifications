import React from 'react'
import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Path,
  G,
  Use,
  Mask,
} from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: title, filter */

const ThirdPageSvg = props => (
  <Svg width="100%" height="100%" viewBox="0 0 171 145" {...props}>
    <Defs>
      <LinearGradient
        x1="114.9%"
        y1="67.206%"
        x2="0%"
        y2="38.144%"
        id="prefix__g"
      >
        <Stop stopColor="#FF881B" offset="0%" />
        <Stop stopColor="#FF971B" offset="20%" />
        <Stop stopColor="#FFA51B" offset="40%" />
        <Stop stopColor="#FFB41B" offset="60%" />
        <Stop stopColor="#FFC11B" offset="80%" />
        <Stop stopColor="#FFCF1B" offset="100%" />
      </LinearGradient>
      <LinearGradient
        x1="79.416%"
        y1="159.472%"
        x2="7.984%"
        y2="0%"
        id="prefix__l"
      >
        <Stop stopColor="#FF881B" offset="0%" />
        <Stop stopColor="#FF971B" offset="20%" />
        <Stop stopColor="#FFA51B" offset="40%" />
        <Stop stopColor="#FFB41B" offset="60%" />
        <Stop stopColor="#FFC11B" offset="80%" />
        <Stop stopColor="#FFCF1B" offset="100%" />
      </LinearGradient>
      <LinearGradient x1="99.449%" y1="100%" x2=".551%" y2="0%" id="prefix__k">
        <Stop stopColor="#FF881B" offset="0%" />
        <Stop stopColor="#FF971B" offset="20%" />
        <Stop stopColor="#FFA51B" offset="40%" />
        <Stop stopColor="#FFB41B" offset="60%" />
        <Stop stopColor="#FFC11B" offset="80%" />
        <Stop stopColor="#FFCF1B" offset="100%" />
      </LinearGradient>
      <LinearGradient
        x1="39.601%"
        y1="18.176%"
        x2="100%"
        y2="84.017%"
        id="prefix__j"
      >
        <Stop stopColor="#FF9313" offset="0%" />
        <Stop stopColor="#F60" offset="100%" />
      </LinearGradient>
      <LinearGradient
        x1="39.601%"
        y1="38.873%"
        x2="78.418%"
        y2="61.444%"
        id="prefix__e"
      >
        <Stop stopColor="#FF9313" offset="0%" />
        <Stop stopColor="#F60" offset="100%" />
      </LinearGradient>
      <LinearGradient x1="77.67%" y1="100%" x2="22.33%" y2="0%" id="prefix__d">
        <Stop stopColor="#FF881B" offset="0%" />
        <Stop stopColor="#FF971B" offset="20%" />
        <Stop stopColor="#FFA51B" offset="40%" />
        <Stop stopColor="#FFB41B" offset="60%" />
        <Stop stopColor="#FFC11B" offset="80%" />
        <Stop stopColor="#FFCF1B" offset="100%" />
      </LinearGradient>
      <LinearGradient
        x1="39.601%"
        y1="18.176%"
        x2="78.418%"
        y2="82.731%"
        id="prefix__h"
      >
        <Stop stopColor="#FF9313" offset="0%" />
        <Stop stopColor="#F60" offset="100%" />
      </LinearGradient>
      <Path
        d="M8.216 32.045a6.007 6.007 0 00-6.006 6.002v18.935a6.004 6.004 0 006.006 6.003h51.525a6.007 6.007 0 006.006-6.003V38.047a6.004 6.004 0 00-6.006-6.002H8.216z"
        id="prefix__f"
      />
      <Path
        d="M5.408 0H42.47C52.705 0 61 6.558 61 22.817v50.769C61 78.228 58.633 82 55.703 82H8.61C-1.625 81.988.111 68.81.111 52.551V8.401C.111 3.76 2.486 0 5.408 0z"
        id="prefix__c"
      />
    </Defs>
    <G fill="none" fillRule="evenodd">
      <Path
        d="M64.5 7C32.794 7 7 32.794 7 64.5S32.794 122 64.5 122 122 96.206 122 64.5 96.206 7 64.5 7zM64 120C33.12 120 8 94.88 8 64S33.12 8 64 8s56 25.12 56 56-25.12 56-56 56z"
        fill="#487CD2"
        opacity={0.428}
        filter="url(#prefix__a)"
        transform="translate(8 8)"
      />
      <Path
        d="M73 12c-33.085 0-60 26.915-60 60s26.915 60 60 60 60-26.915 60-60-26.915-60-60-60zm0 118.605c-32.315 0-58.605-26.29-58.605-58.605S40.685 13.395 73 13.395 131.605 39.685 131.605 72 105.315 130.605 73 130.605z"
        fill="#F5A623"
        fillRule="nonzero"
        opacity={0.136}
      />
      <Path
        d="M72.5 8C36.934 8 8 36.934 8 72.5S36.934 137 72.5 137 137 108.066 137 72.5 108.066 8 72.5 8zm-.592 126.633c-34.587 0-62.725-28.138-62.725-62.725S37.321 9.183 71.908 9.183s62.725 28.138 62.725 62.725-28.138 62.725-62.725 62.725z"
        fill="#F5A623"
        fillRule="nonzero"
        opacity={0.336}
      />
      <G fillRule="nonzero">
        <G transform="translate(42 31)">
          <Use fill="#000" filter="url(#prefix__b)" xlinkHref="#prefix__c" />
          <Use fill="url(#prefix__d)" xlinkHref="#prefix__c" />
        </G>
        <Path
          d="M53.76 46C51.857 46 51 47.236 51 48.757v3.486c0 1.521.857 2.757 2.76 2.757h34.48c1.903 0 2.76-1.236 2.76-2.757v-3.486C91 47.236 90.143 46 88.24 46H53.76zM56.07 62c-1.427 0-2.07.55-2.07 1.225v1.55c0 .676.643 1.225 2.07 1.225h25.86c1.427 0 2.07-.55 2.07-1.225v-1.55C84 62.55 83.357 62 81.93 62H56.07zM56.07 70c-1.427 0-2.07.55-2.07 1.225v1.55c0 .676.643 1.225 2.07 1.225h25.86c1.427 0 2.07-.55 2.07-1.225v-1.55C84 70.55 83.357 70 81.93 70H56.07zM46.36 42.338c-.748 0-1.36-.66-1.36-1.465v-.992C45 37.74 46.617 36 48.605 36h7.035c.748 0 1.36.659 1.36 1.464 0 .806-.612 1.465-1.36 1.465h-7.035c-.492 0-.885.431-.885.952v.992c0 .806-.612 1.465-1.36 1.465zm0 5.662c-.748 0-1.36-.659-1.36-1.464v-.855c0-.805.612-1.464 1.36-1.464.748 0 1.36.659 1.36 1.464v.855c0 .805-.612 1.464-1.36 1.464z"
          fill="#FFF"
        />
      </G>
      <G transform="translate(105 36)">
        <Path
          d="M7.209 25.415a5 5 0 00-4.999 5.008v27.553a5.007 5.007 0 004.999 5.009h53.54a5 5 0 004.998-5.009V30.423a5.007 5.007 0 00-4.999-5.008H7.208z"
          fill="url(#prefix__e)"
        />
        <Mask id="prefix__i" fill="#fff">
          <Use xlinkHref="#prefix__f" />
        </Mask>
        <Use fill="url(#prefix__g)" xlinkHref="#prefix__f" />
        <Path
          fill="url(#prefix__h)"
          mask="url(#prefix__i)"
          transform="rotate(-4 16.575 45.857)"
          d="M-1.105 63.537v-35.36l35.36 17.68z"
        />
        <Path
          fill="url(#prefix__j)"
          mask="url(#prefix__i)"
          transform="scale(-1 1) rotate(-4 0 1517.252)"
          d="M33.702 63.537v-35.36l35.36 17.68z"
        />
        <Path
          d="M13.007 0L5.14 9.057v35.009h56.767V8.997A8.994 8.994 0 0052.91 0H13.007z"
          fill="#ECF0F1"
        />
        <Path fill="#EFC22A" d="M12.762 0v8.84H5z" />
        <Path fill="#D4D8D8" d="M5.097 18V9H13z" />
        <Path
          d="M6.384 59.361c-2.918 1.56-5.163.136-5.015-3.163l.999-22.29c.148-3.306 2.641-4.743 5.588-3.197l20.933 10.98c2.938 1.542 2.952 4.058.037 5.616L6.384 59.361z"
          fill="url(#prefix__k)"
          transform="rotate(-3 16.232 45.042)"
        />
        <Path
          d="M42.883 60.105c-2.938 1.545-5.294.115-5.262-3.199l.223-23.119c.032-3.311 2.349-4.603 5.18-2.881L63.592 43.41c2.83 1.72 2.743 4.365-.197 5.91L42.883 60.106z"
          fill="url(#prefix__l)"
          transform="rotate(-180 51.639 45.435)"
        />
      </G>
    </G>
  </Svg>
)

export default ThirdPageSvg
