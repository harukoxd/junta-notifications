import React from 'react'
import Svg, {
  Defs,
  LinearGradient,
  Stop,
  G,
  Circle,
  Path,
} from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: title */

const PhoneRegister = props => (
  <Svg  width="100%" height="100%" viewBox="0 0 91 91" {...props}>
    <Defs>
      <LinearGradient
        x1="66.694%"
        y1="87.976%"
        x2="35.081%"
        y2="34.287%"
        id="prefix__a"
      >
        <Stop stopColor="#2692FF" offset="0%" />
        <Stop stopColor="#00BEFF" offset="100%" />
      </LinearGradient>
    </Defs>
    <G transform="translate(1 1)" fill="none" fillRule="evenodd">
      <Circle
        cx={40.5}
        cy={38.5}
        r={35.5}
        transform="translate(4 6)"
        fill="url(#prefix__a)"
      />
      <G opacity={0.829} fill="#FAF8F4">
        <Path d="M51.2 62.027H36.806a2.116 2.116 0 01-2.113-2.113v-5.6h18.62v5.6a2.116 2.116 0 01-2.113 2.113zm-16.508-9.404h18.62V32.209h-18.62v20.414zm2.114-25.93H51.2c1.165 0 2.113.948 2.113 2.113v1.71h-18.62v-1.71c0-1.165.948-2.113 2.113-2.113zM51.2 25H36.806A3.811 3.811 0 0033 28.806v31.108a3.811 3.811 0 003.806 3.806H51.2a3.81 3.81 0 003.806-3.806V28.806A3.81 3.81 0 0051.2 25z" />
        <Path d="M44.003 56.401a1.853 1.853 0 000 3.703 1.853 1.853 0 000-3.703" />
      </G>
      <Circle
        stroke="#487CD2"
        opacity={0.414}
        cx={44.352}
        cy={44.352}
        r={44.352}
      />
      <Circle
        stroke="#487CD2"
        opacity={0.414}
        cx={44.013}
        cy={44.013}
        r={40.013}
      />
    </G>
  </Svg>
)

export default PhoneRegister
