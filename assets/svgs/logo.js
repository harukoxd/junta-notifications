import React from 'react'
import Svg, { LinearGradient, Stop, Path, G } from 'react-native-svg'
/* SVGR has dropped some elements not supported by react-native-svg: style */
const Logo = props => (
  <Svg
    id="prefix__Layer_1"
    x={0}
    y={0}
    width="80%"
    height="80%"

    viewBox="0 0 40 40"
    xmlSpace="preserve"
    {...props}
  >
    <LinearGradient
      id="prefix__SVGID_1_"
      gradientUnits="userSpaceOnUse"
      x1={20}
      y1={-0.028}
      x2={20}
      y2={38.121}
    >
      <Stop offset={0} stopColor="#3f4d69" />
      <Stop offset={1} stopColor="#262d33" />
    </LinearGradient>
    <Path
      d="M37 26.3V13.7c0-2.1-1.1-4.1-3-5.2L23 2.2c-1.9-1.1-4.1-1.1-6 0L6 8.5c-1.9 1.1-3 3.1-3 5.2v12.6c0 2.1 1.1 4.1 3 5.2l11 6.3c1.9 1.1 4.1 1.1 6 0l11-6.3c1.8-1.1 3-3 3-5.2z"
      fill="url(#prefix__SVGID_1_)"
    />
    <LinearGradient
      id="prefix__SVGID_2_"
      gradientUnits="userSpaceOnUse"
      x1={1581.74}
      y1={-148.627}
      x2={2037.908}
      y2={307.541}
    >
      <Stop offset={0} stopColor="#455df0" />
      <Stop offset={1} stopColor="#345d9d" />
    </LinearGradient>
    <Path
      fill="none"
      stroke="url(#prefix__SVGID_2_)"
      strokeMiterlimit={10}
      d="M1661.2-227.1h297.2V386h-297.2z"
    />
    <LinearGradient
      id="prefix__SVGID_3_"
      gradientUnits="userSpaceOnUse"
      x1={1297.516}
      y1={-458.814}
      x2={1708.553}
      y2={-47.778}
    >
      <Stop offset={0} stopColor="#455df0" />
      <Stop offset={1} stopColor="#345d9d" />
    </LinearGradient>
    <Path
      fill="none"
      stroke="url(#prefix__SVGID_3_)"
      strokeWidth={0.869}
      strokeMiterlimit={10}
      d="M1661.2-1.7l-454.6-276V-367h454.6z"
    />
    <G id="prefix__Canvas">
      <G id="prefix__xrp-symbol-black">
        <Path
          id="prefix__Vector"
          className="prefix__st3"
          d="M27.4 11.3h3l-6.3 6.3c-2.3 2.3-6 2.3-8.3 0l-6.3-6.3h3l4.8 4.8c1.4 1.4 3.8 1.4 5.2 0l4.9-4.8z"
        />
        <Path
          id="prefix__Vector_2"
          className="prefix__st3"
          d="M12.5 28.7h-3l6.4-6.3c2.3-2.3 6-2.3 8.3 0l6.4 6.3h-3l-4.8-4.8c-1.4-1.4-3.8-1.4-5.2 0l-5.1 4.8z"
        />
      </G>
    </G>
  </Svg>
)

export default Logo
