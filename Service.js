class Service{
  constructor(json){
    for(var i = 0;i<json.length;i++){
      if(json[i]["key"]["_text"] == "tipo")
          this.tipo = json[i]["value"]["_text"]
      else if(json[i]["key"]["_text"] == "id")
          this.id = json[i]["value"]["_text"]
      else if(json[i]["key"]["_text"] == "ide")
          this.ide = json[i]["value"]["_text"]
      else if(json[i]["key"]["_text"] == "sadesi")
          this.sadesi = json[i]["value"]["_text"]
      else if(json[i]["key"]["_text"] == "url")
          this.url = json[i]["value"]["_text"]
      else if(json[i]["key"]["_text"] == "desc")
          this.desc = json[i]["value"]["_text"]
      else if(json[i]["key"]["_text"] == "url_baja")
          this.url_baja = json[i]["value"]["_text"]
      else if(json[i]["key"]["_text"] == "url_alta")
          this.url_alta = json[i]["value"]["_text"]
      else if(json[i]["key"]["_text"] == "method")
          this.method = json[i]["value"]["_text"]
    }
  }
}

export default Service;
