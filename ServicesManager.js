import Service from './Service'

class ServiceManager{
  constructor(json){
    this.listServices = []
    var body = json["soapenv:Envelope"]["soapenv:Body"]
    if(body["multiRef"] == undefined)
      alert("Error obtening services")
    else {
      var listServices = body["multiRef"]
      for(var i = 0;i<listServices.length;i++)
      {
          if(listServices[i]["_attributes"]["xsi:type"] == "ns2:Vector")
            continue;
          var service= new Service(listServices[i]["item"])
          this.listServices.push(service)
      }
    }
  }
}

export default ServiceManager;
